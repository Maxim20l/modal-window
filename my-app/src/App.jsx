import React, { Component, Fragment } from 'react';
import './style.scss';


import { ModalDeleteElement } from './components/modalDeleteElement';
import { ModalRandom } from './components/modalRandom';
import { ButtonOpenModalDelete } from './components/buttonOpenModalDelete';
import { ButtonOpenRandomModal } from './components/buttonOpenRandomModal';


class App extends Component {
    state = {
        isModalDelete: false,
        isModalRandom: false
    }
    openModalToDelete = (event) => {
        event.stopPropagation()
        this.setState((prevState) => {
            return {
                ...prevState,
                isModalDelete: !prevState.isModalDelete
            }
        })
    }

    closeModalDelete = (event) => {
        event.stopPropagation()
        this.setState((prevState) => {
            return {
                ...prevState,
                isModalDelete: !prevState.isModalDelete
            }
        })
    }

    openRandomModal = (event) => {
        event.stopPropagation()
        this.setState((prevState) => {
            return {
                ...prevState,
                isModalRandom: !prevState.isModalRandom
            }
        })
    }

    closeRandomModal = (event) => {
        event.stopPropagation()
        this.setState((prevState) => {
            return {
                ...prevState,
                isModalRandom: !prevState.isModalRandom
            }
        })
    }

    closeModals = (event) => {


        if (this.state.isModalRandom === true) {
            event.stopPropagation();
            if (event.target.className === 'modal-wrapper') {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        isModalRandom: !prevState.isModalRandom
                    }
                })
            }
        }
        if (this.state.isModalDelete === true) {
            event.stopPropagation();
            if (event.target.className === 'modal-wrapper') {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        isModalDelete: !prevState.isModalDelete
                    }
                })
            }
        }
    }

    render() {
        const { isModalDelete, isModalRandom } = this.state;
        return (
            <div className='content'>
                <div className='button-container'>
                    <ButtonOpenModalDelete openModalToDelete={this.openModalToDelete} text={'Open first modal'} style={{ backgroundColor: 'rgb(224,18,79)' }} />
                    <ButtonOpenRandomModal openRandomModal={this.openRandomModal} text={'Open second modal'} style={{ backgroundColor: 'rgb(93,224,131)' }} />
                </div>
                {isModalDelete && <ModalDeleteElement closeModalDelete={this.closeModalDelete} closeModals={this.closeModals} />}
                {isModalRandom && <ModalRandom closeRandomModal={this.closeRandomModal} closeModals={this.closeModals} />}
            </div>
        )
    }
}



export default App;