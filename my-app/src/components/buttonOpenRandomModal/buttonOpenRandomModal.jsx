import { Component } from "react";


class ButtonOpenRandomModal extends Component {
    render () {
        const {text, openRandomModal, style} = this.props;

        return (
            <button className='openModalRandom btnModal' onClick={openRandomModal} style={style}>{text}</button>
        )
    }
}

export default ButtonOpenRandomModal;