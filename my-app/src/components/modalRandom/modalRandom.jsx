import { Component } from "react";
import { createUseStyles, ThemeProvider, useTheme } from "react-jss";


class ModalRandom extends Component {
    render() {
        const { closeRandomModal, closeModals } = this.props;
        return (
            <div className="modal-wrapper" onClick={closeModals}>
                <div className="modalRandom">
                    <div className="headerModal">
                        <p className='headerModalQuestion'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        <div className='closeModal' onClick={closeRandomModal}></div>
                    </div>
                    <div className="informPartOfModal">
                        <p className='informText'>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                        <p className='informQuestion'>Lorem, ipsum dolor sit amet consectetur </p>
                        <div className='btnsModalClose'>
                            <button className="btnSomeEction" type="button">Ok</button>
                            <button className="btnSomeEction" type="button" onClick={closeRandomModal}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

export default ModalRandom;