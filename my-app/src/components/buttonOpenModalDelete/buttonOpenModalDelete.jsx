import { Component } from "react";


class ButtonOpenModalDelete extends Component {

    render() {
        const {style, text, openModalToDelete} = this.props;

        return (
            <button className='openModalDelete btnModal' onClick={openModalToDelete} style={style}>{text}</button>
        )
    }

}

export default ButtonOpenModalDelete;