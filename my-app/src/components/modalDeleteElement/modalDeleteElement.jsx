import { Component } from "react";



class ModalDeleteElement extends Component {

    render() {
        const { closeModalDelete, closeModals } = this.props;
        return (
            <div className="modal-wrapper" onClick={closeModals}>
                <div className="modalDeleteElement">
                    <div className="headerModal">
                        <p className='headerModalQuestion'>Do you want too delete this file?</p>
                        <div className='closeModal' onClick={closeModalDelete}></div>
                    </div>
                    <div className="informPartOfModal">
                        <p className='informText'>Onse you delete this file, it won`t be possible to undo this action.</p>
                        <p className='informQuestion'>Are you sure you want to delete this?</p>
                        <div className='btnsModalClose'>
                            <button className="btnModalAgree" type="button">Ok</button>
                            <button className="btnModalDisagree" type="button" onClick={closeModalDelete}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default ModalDeleteElement;